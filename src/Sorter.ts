// interface Sortable {
//   length: number;
//   compare(leftIndex: number, rightIndex: number): boolean;
//   swap(leftIndex: number, rightIndex: number): void;
// }
export abstract class Sorter {
  // * abstract method - existing in the future or implemented by child class
  abstract compare(leftIndex: number, rightIndex: number): boolean;
  abstract swap(leftIndex: number, rightIndex: number): void;
  abstract length: number;

  // constructor(public collection: Sortable) {}

  // ! BAD WAY
  // constructor(public collection: number[] | string) {}

  // * пример: если сортировать linked list напрямую (т.е. в этом классе) - то будет громоздко и сложно; создаем отдельный класс, который имплементирует заданный интерфейс, он сам регламентирует внутренности, вместо того, чтобы их здесь обрабатывать
  sort(): void {
    const { length } = this;

    // if (this.collection instanceof Array) {

    for (let i = 0; i < length; i++) {
      for (let j = 0; j < length - i - 1; j++) {
        if (this.compare(j, j + 1)) {
          this.swap(j, j + 1);
        }
      }
    }

    // }
    // ! BAD WAY
    // if (typeof this.collection === 'string') {

    // }
  }
}
