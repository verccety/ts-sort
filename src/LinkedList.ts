import { Sorter } from "./Sorter";

class Node {
  next: Node | null = null;
  constructor(public data: number) {}
}

export class LinkedList extends Sorter {
  head: Node | null = null;

  add(data: number): void {
    const node = new Node(data);
    // если список пустой - ставим текущий node как первый
    if (!this.head) {
      this.head = node;
      return;
    }

    let tail = this.head;
    // пока есть след. node -> продвигаем tail
    while (tail.next) {
      tail = tail.next;
    }
    // ставим новый node как последний в списке
    tail.next = node;
  }

  get length(): number {
    if (!this.head) return 0;

    let length = 1;
    let node = this.head;
    // пока есть след node -> увеличиваем счетчик кол-ва узлов
    while (node.next) {
      length++;
      node = node.next;
    }

    return length;
  }

  at(index: number): Node {
    if (!this.head) throw new Error("Index out of bounds");

    // 0 - т.к. считаем с 0 индекса
    let counter = 0;

    let node: Node | null = this.head;
    while (node) {
      if (counter === index) return node;
      counter++;
      node = node.next;
    }
    throw new Error("Index out of bounds");
  }

  compare(leftIndex: number, rightIndex: number): boolean {
    if (!this.head) throw new Error("List is empty");

    return this.at(leftIndex).data > this.at(rightIndex).data;
  }
  // меняем местами не сами узлы (т.к. сложно), а только их значения
  swap(leftIndex: number, rightIndex: number): void {
    const leftNode = this.at(leftIndex);
    const rightNode = this.at(rightIndex);

    [leftNode.data, rightNode.data] = [rightNode.data, leftNode.data];
  }

  print(): void {
    if (!this.head) return;

    let node: Node | null = this.head;
    while (node) {
      console.log(node.data);
      node = node.next;
    }
  }
}
